#include "cla_parser.hxx"
#include <tuple>

std::tuple<std::string, std::string, std::string, std::string, double>
ovax::cla_parser::operator()(int argc, char* argv[])
{
  std::string video;
  std::string prototxt = default_prototxt_.data();
  std::string weights = default_weights_.data();
  std::string classes = default_classes_.data();
  auto threshold = default_threshold_;
  auto option_index = 0;

  while (true) {
    if (getopt_long(argc, argv, "", long_options_, &option_index) == -1) {
      break;
    }
    switch (static_cast<options_sequence_>(option_index)) {
      case options_sequence_::video:
        video = optarg;
        break;
      case options_sequence_::prototxt:
        prototxt = optarg;
        break;
      case options_sequence_::weights:
        weights = optarg;
        break;
      case options_sequence_::classes:
        classes = optarg;
        break;
      case options_sequence_::threshold:
        threshold = std::stod(optarg);
        break;
    }
  }

  return std::make_tuple(video, prototxt, weights, classes, threshold);
}