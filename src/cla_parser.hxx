#pragma once

#include <array>
#include <getopt.h>

namespace ovax {
class cla_parser
{
public:
  std::tuple<std::string, std::string, std::string, std::string, double>
  operator()(int argc, char* argv[]);

private:
  enum class options_sequence_
  {
    video,
    prototxt,
    weights,
    classes,
    threshold
  };

  static constexpr option long_options_[]{
    { "video", required_argument, nullptr, 0 },
    { "prototxt", optional_argument, nullptr, 0 },
    { "weights", optional_argument, nullptr, 0 },
    { "classes", optional_argument, nullptr, 0 },
    { "threshold", optional_argument, nullptr, 0 }
  };
  static constexpr std::string_view default_prototxt_ =
    "mobile_net_ssd.prototxt";
  static constexpr std::string_view default_weights_ =
    "mobile_net_ssd.caffemodel";
  static constexpr std::string_view default_classes_ = "ssd_voc.names";
  static constexpr double default_threshold_ = 0.25;
};
} // namespace ovax
