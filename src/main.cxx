#include <chrono>
#include <fstream>
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "cla_parser.hxx"

std::vector<std::string>
read_classes(std::string classes_filename)
{
  std::ifstream classes_file(classes_filename, std::ios::in);
  if (!classes_file.is_open()) {
    throw std::runtime_error("Invalid classes file: " + classes_filename);
  }

  std::string class_name;
  std::vector<std::string> classes;
  while (std::getline(classes_file, class_name)) {
    classes.push_back(class_name);
  }

  return classes;
}

int
main(int argc, char* argv[])
{
  cv::ocl::setUseOpenCL(true);
  ovax::cla_parser cla_parser;
  auto [video, prototxt, weights, classes_filename, threshold] =
    cla_parser(argc, argv);

  cv::VideoCapture video_capture(video);
  if (!video_capture.isOpened()) {
    throw std::runtime_error("Invalid video file: " + video);
  }

  auto net = cv::dnn::readNetFromCaffe(prototxt, weights);
  auto classes = read_classes(classes_filename);

  auto delay = 40;
  cv::namedWindow("ssd", cv::WINDOW_NORMAL);
  while (true) {
    cv::Mat frame;
    if (!video_capture.read(frame)) {
      break;
    }

    // resize frame for prediction
    cv::Mat frame_resized;
    auto blob_size = cv::Size(300, 300);
    cv::resize(frame, frame_resized, blob_size);

    // MobileNet requires fixed dimensions for input image(s)
    // so we have to ensure that it is resized to 300x300 pixels.
    // set a scale factor to image because network the objects has differents
    // size. We perform a mean subtraction(127.5, 127.5, 127.5) to normalize the
    // input; after executing this command our "blob" now has the shape: (1, 3,
    // 300, 300)
    auto blob = cv::dnn::blobFromImage(frame_resized,
                                       0.007843,
                                       blob_size,
                                       cv::Scalar(127.5, 127.5, 127.5),
                                       false);
    net.setInput(blob, "data");
    auto begin = std::chrono::steady_clock::now();
    auto raw_detection = net.forward("detection_out");
    auto end = std::chrono::steady_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                   .count()
              << std::endl;

    cv::Mat detection_matrix(raw_detection.size[2],
                             raw_detection.size[3],
                             CV_32F,
                             raw_detection.ptr<float>());

    for (auto i = 0; i < detection_matrix.rows; i++) {
      auto confidence = detection_matrix.at<float>(i, 2);
      if (confidence < threshold) {
        continue;
      }

      auto class_id = static_cast<int>(detection_matrix.at<float>(i, 1));
      auto left_bottom_x =
        static_cast<int>(detection_matrix.at<float>(i, 3) * frame.cols);
      auto left_bottom_y =
        static_cast<int>(detection_matrix.at<float>(i, 4) * frame.rows);
      auto right_top_x =
        static_cast<int>(detection_matrix.at<float>(i, 5) * frame.cols);
      auto right_top_y =
        static_cast<int>(detection_matrix.at<float>(i, 6) * frame.rows);

      cv::Rect object_rect(left_bottom_x,
                           left_bottom_y,
                           right_top_x - left_bottom_x,
                           right_top_y - left_bottom_y);

      auto class_name = classes[class_id];
      auto class_hash = std::hash<std::string>{}(class_name);
      auto red = (class_hash & 0xff) << 1;
      auto green = ((class_hash >> 3) & 0xff) << 1;
      auto blue = ((class_hash >> 6) & 0xff) << 1;
      auto class_color = cv::Scalar(blue, green, red);

      cv::rectangle(frame, object_rect, class_color, 4);

      auto baseline = 0;
      auto label_size =
        cv::getTextSize(class_name, cv::FONT_HERSHEY_DUPLEX, 1.0, 2, &baseline);

      cv::rectangle(
        frame,
        cv::Rect(object_rect.tl(),
                 cv::Size(label_size.width, label_size.height + baseline)),
        class_color,
        cv::FILLED);

      cv::putText(frame,
                  class_name,
                  object_rect.tl() + cv::Point(0, label_size.height),
                  cv::FONT_HERSHEY_SIMPLEX,
                  1.0,
                  cv::Scalar::all(0),
                  2);
    }

    cv::imshow("ssd", frame);

    auto keyboard = cv::waitKey(delay);
    switch (keyboard) {
      case 'p':
        delay = delay == 40 ? 0 : 40;
        break;
      case 'q':
        return 0;
      case 27:
        return 0;
    }
  }
  return 0;
}
